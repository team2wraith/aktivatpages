<!DOCTYPE html>
<html lang="en">
    <head>
		<meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> 
        <meta name="description" content="" />
        <meta name="keywords" content="" />
		<title>Aktivat App</title>
		<!---<link rel="stylesheet" href="//f.fontdeck.com/s/css/YSnkK+F0PfsHj8uOkcB110k+0wc/DOMAIN_NAME/48004.css" type="text/css" media="all" />-->
		<link rel="stylesheet" href="css/styles.css" media="all" />
		<link rel="stylesheet" href="css/styles.css" media="all" />
		<link rel="stylesheet" href="//f.fontdeck.com/s/css/YSnkK+F0PfsHj8uOkcB110k+0wc/DOMAIN_NAME/48004.css" type="text/css" />
		<script src="js/iconic.min.js"></script>
		<script src="js/modernizr.custom.js"></script>
		<style>
			body{
				background: rgb(62,70,81); /* Old browsers */
				background: -moz-radial-gradient(center, ellipse cover,  rgba(62,70,81,1) 1%, rgba(29,36,46,1) 95%); /* FF3.6+ */
				background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(1%,rgba(62,70,81,1)), color-stop(95%,rgba(29,36,46,1))); /* Chrome,Safari4+ */
				background: -webkit-radial-gradient(center, ellipse cover,  rgba(62,70,81,1) 1%,rgba(29,36,46,1) 95%); /* Chrome10+,Safari5.1+ */
				background: -o-radial-gradient(center, ellipse cover,  rgba(62,70,81,1) 1%,rgba(29,36,46,1) 95%); /* Opera 12+ */
				background: -ms-radial-gradient(center, ellipse cover,  rgba(62,70,81,1) 1%,rgba(29,36,46,1) 95%); /* IE10+ */
				background: radial-gradient(ellipse at center,  rgba(62,70,81,1) 1%,rgba(29,36,46,1) 95%); /* W3C */
				filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#3e4651', endColorstr='#1d242e',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */

			}
		</style>
	</head>
<body>

	<div id="av-container" class="av-container">
		<div class="inner-wrapper">
			<aside id="av-menu" class="av-menu">
				<div class="logo">
					<a href="#"><img src="images/aktivat.svg" alt="Aktivat" /></a>
				</div>
				<nav>
					<ul>
						<li><a href="#"><img data-src="images/svg/smart/graph.svg" class="iconic iconic-aa" alt="Analytics">
							<span>Analytics</span></a></li>
						<li><a href="#"><img data-src="images/svg/smart/audio.svg" class="iconic iconic-tr" alt="Triggers"><span>Triggers</span></a></li>
						<li><a href="#"><img data-src="images/svg/smart/beaker.svg" class="iconic iconic-op" alt="Optimizer"><span>Optimizer</span></a></li>
						<li><a href="#"><img data-src="images/svg/smart/cogs.svg" class="iconic" alt="Settings"></a></li>
						<li><a href="#"><img data-src="images/svg/smart/question-mark.svg" class="iconic" alt="Help"></a></li>
						<li><a href="#">| Aktivat</a></li>
						<li><a href="#"><img data-src="images/ProductureP.svg" class="iconic" alt="Producture"></a></li>
					</ul>
				</nav>
			</aside>
			<section id="av-content" class="av-content">
				<div class="av-content-inner">
					<h2>Let's start. It will take 2 minutes.</h2>
					<form action="#" class="sign-up">
						<input type="text" placeholder="Your first name" tabindex="1" required> <br/>
						<input type="text" placeholder="Your email" tabindex="2" required> <br/>
						<input type="text" placeholder="Your password" tabindex="3" required> <br/>
						<input type="submit" name="submit" value="Next">
					</form>
				</div>
			</section>
		</div>
	</div>
		<script src="iconic.min.js"></script>
</body>
</html>s